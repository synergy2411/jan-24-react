# Break Timing

- Tea Break : 11:00AM (15 mins)
- Lunch : 1:00PM (45 mins)
- Tea Break 3:30PM (15 mins)

# JavaScript

- Dynamic Language : Types are determined at runtime
- Loosely typed
- Scripting Language
- Asynchronous : task executed in next cycle of code execution
- ES6+ features : OOPS - classes / object, Arrow function, Destructuring, Rest,spread, template syntax; Object.assign(), values(), entries(), getOwnPropertyDescriptor, padding function etc (BABEL)
- Event based
- Client (Browser) as well as Server side (Node Platform)
- Compiled (AST -> Abstract Syntax Tree, JIT Compiler) & Interpreted
- Single threaded / Non-blocking
- OOPS concepts / Object based / Functional Programming
- Various Design Patterns

# JavaScript Types

- Primitive : number, string, boolean, symbol, bigint
- Reference : object, array, date, function

# Full Stack Apps

- MEAN : MONGODB EXPRESS ANGULAR NODE
- MERN : MONGODB EXPRESS REACT NODE

# JavaScript Async behaviour

- All Synchronous code will execute first
- Asynchronous code will execute (in next cycle)
  > MicroTasks : Promises, queueMicrotask()
  > MacroTasks : all long running operation (eg. timer, read/write file, sockets)

# BABEL -> transpiler -> JSX into JavaScript

- nextgen JavaScript features, today
- ES13 -> ES6 / ES5

# JavaScript Libraries / Frameworks

- AngularJS : v1, Library; Components
- Angular : v2 onwards; Angular compiler, Framework; One-way data binding, Components based, Routing - SPA, Forms Validation, Guards : Routing Module, Lazy Loading : Webpack, Services - State Management, RxJS - Observable API, PB + EB = 2way; [(ngModel)]; MVC ; library, MFE etc

- jQuery : DOM manipulation, Animation, Remote Server Call (AJAX Calls)
- KnockoutJS : MVVM Pattern, 2 way data binding
- Backbone: CLient side MVC
- EmberJS : Framework; frequent API changes
- VueJS : Framework; No Corporate Care-taker; "Evan You"; still evolving

- React : light-weight (30kb); Library: View only; to render the UI quickly and efficiently

  > State Management : redux, Redux Toolkit
  > SPA : react-router-dom
  > Form Validation : formik, react-hook-form, yup etc
  > Lazy loading : webpack
  > RxJS Library for Observables
  > One way data-binding (state variable + callback (onChange))
  > Components
  > Vast community
  > Huge eco-system
  > GraphQL API
  > NextJS : Server-Side Framework for React
  > Gatsby
  > Native Apps

- Data binding : sync of variable data with view
- Data Flow : unidirectional

-> Parent Comp [data] -> Child Comp [Props]

# Web Design Principles - ATOMIC DESIGN

- Atom : one button, one input element
- Molecule : comb of atoms. eg. SearchBar -> one input element + One Button
- Organism : comb of molecules. eg. NavigationBar -> Various links + SearchBar
- Template : comb of organism. eg. Form
- Page : a complete web page

# Thinking in React way :

- if reusable piece of code, make it a component

# React CLI Tool

- npx create-react-app frontend
- cd frontend
- npm start

- npm install @babel/plugin-proposal-private-property-in-object -D
- npm install bootstrap

# JSX Restriction

- Should have only one root element
- Should not use javascript reserved keywords
